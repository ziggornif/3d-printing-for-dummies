---
marp: true
theme: gaia
_class: lead
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
style: |
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
    }
---

# Découverte de l'impression 3D

![bg left:40% 80%](./assets/logo.png)

---

# Au départ le projet Reprap

RepRap : Replication Rapid prototyper

Projet universitaire visant à créer des imprimantes 3D autoréplicative et libres (licence GPL)

La base des imprimantes cartésiennes FDM (Fused deposition modeling)que nous pouvons retrouver aujourd'hui.

---

# Exemples d'imprimantes Reprap

![center](./assets/RepRap_Mendel.jpg)

---

![width:500px center](./assets/Reprap_Darwin.jpg)

---

# FDM ?

FDM : Fused deposition modeling

Technologie qui consiste à déposer de la matière à l'état liquide couche par couche

![bg right width:500px](./assets/Extruder_lemio.svg)

---

# Les différents types d'imprimantes 3D

**Grand public / Usage professionnel**

- Imprimantes FDM cartésiennes
- Imprimantes FDM delta
- Imprimantes résine SLA / MSLA

**Usage professionnel**
- Imprimantes métal
- Bio imprimantes

---

# Imprimantes FDM cartésiennes

![center w:500px](./assets/prusa.jpg)

---

# Imprimantes FDM delta

![center w:500px](./assets/flsun.jpg)

---

# Exemples

https://www.youtube.com/watch?v=m_QhY1aABsE

https://www.youtube.com/watch?v=Mmd2wv1BkBk

---

# Imprimantes résine SLA

![center w:800px](./assets/03062019_daguerre_1_565.webp)

---

# Exemple

Vidéo du slicer + écran : https://gitlab.com/ziggornif/3d-printing-for-dummies/-/blob/main/assets/2023-03-12%2012-28-33.mp4

https://www.youtube.com/watch?v=08rGK-9fIzM

---

# Quelles applications ?

![bg right w:600px](./assets/shea2.jpg)

- Robotique
- Aérospatiale
- Santé
- Éducation
- Prototypage / Conception de produits
- Makers / DIY

---

# Modélisation

Logiciels de modélisation 3D que j'ai eu l'occasion d'utiliser :

- Tinkercad : très basique
- Fusion 360 : complet mais usage gratuit limité
- Freecad : complet et open source mais interface complexe au début

---

# Slicers

Le classique : 

UltiMaker Cura : https://ultimaker.com/software/ultimaker-cura

Mon préféré :

Prusa Slicer : https://www.prusa3d.com/page/prusaslicer_424/

Le petit nouveau qui est un fork de Prusa slicer :

Super Slicer : https://github.com/supermerill/SuperSlicer

---

# Firmware

Le plus répendu : Marlin

https://github.com/MarlinFirmware/Marlin

![bg right w:300px](./assets/marlin-outrun-nf-500.png)

Le nouveau qui monte : Klipper

https://github.com/Klipper3d/klipper

---

# Démo

![bg w:400px center](./assets/pngegg.png)

---

<br >
<br >
<br >
<br >
<p style="text-align:center; vertical-align:middle;">
<strong>Merci pour votre participation 🙏</strong>
</p>